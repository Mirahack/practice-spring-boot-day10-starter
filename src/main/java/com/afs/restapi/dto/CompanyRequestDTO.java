package com.afs.restapi.dto;

import java.util.List;

public class CompanyRequestDTO {
    private String name;
    private List<EmployeeRequestDTO> employees;

    public CompanyRequestDTO() {
    }

    public CompanyRequestDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EmployeeRequestDTO> getEmployees() {
        return employees;
    }

    public void setEmployees(List<EmployeeRequestDTO> employees) {
        this.employees = employees;
    }
}
