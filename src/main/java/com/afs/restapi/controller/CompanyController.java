package com.afs.restapi.controller;

import com.afs.restapi.dto.CompanyRequestDTO;
import com.afs.restapi.dto.CompanyResponseDTO;
import com.afs.restapi.dto.EmployeeReponseDTO;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.service.CompanyService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/companies")
public class CompanyController {
    private final CompanyService companyService;

    public CompanyController(CompanyService companyService) {
        this.companyService = companyService;
    }

    @GetMapping
    public List<CompanyResponseDTO> getAll() {
        return companyService.getAll();
    }

    @GetMapping(params = {"page", "size"})
    public List<CompanyResponseDTO> getAll(
            @RequestParam(required = false) Integer page,
            @RequestParam(required = false) Integer size
    ) {
        return companyService.getAll(page, size);
    }

    @GetMapping("/{companyId}")
    public CompanyResponseDTO get(@PathVariable Integer companyId) {
        return companyService.findById(companyId);
    }

    @GetMapping("/{companyId}/employees")
    public List<EmployeeReponseDTO> getEmployees(@PathVariable Integer companyId) {
        return companyService.getEmployees(companyId);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CompanyResponseDTO create(@RequestBody CompanyRequestDTO companyRequest) {
        return companyService.create(companyRequest);
    }

    @DeleteMapping("/{companyId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCompany(@PathVariable Integer companyId) {
        companyService.deleteCompany(companyId);
    }

    @PutMapping("/{companyId}")
    public CompanyResponseDTO update(@PathVariable Integer companyId, @RequestBody CompanyRequestDTO companyRequest) {
        return companyService.update(companyId, companyRequest);
    }
}
