package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeReponseDTO;
import com.afs.restapi.dto.EmployeeRequestDTO;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeReponseDTO> findAll() {
        return employeeRepository
                .findAllByStatusTrue()
                .stream()
                .map(EmployeeMapper::convertEmployeeToEmployeeResponseDTO)
                .collect(Collectors.toList());
    }

    public EmployeeReponseDTO update(int id, EmployeeRequestDTO toUpdate) {
        Employee employee = employeeRepository.findByIdAndStatusTrue(id).orElseThrow(EmployeeNotFoundException::new);
        if (toUpdate.getAge() != null) {
            employee.setAge(toUpdate.getAge());
        }
        if (toUpdate.getSalary() != null) {
            employee.setSalary(toUpdate.getSalary());
        }
        employeeRepository.save(employee);
        return EmployeeMapper.convertEmployeeToEmployeeResponseDTO(employee);
    }

    public EmployeeReponseDTO findById(int id) {
        return EmployeeMapper.convertEmployeeToEmployeeResponseDTO(employeeRepository.findByIdAndStatusTrue(id)
                .orElseThrow(EmployeeNotFoundException::new));
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGenderAndStatusTrue(gender);
    }

    public List<EmployeeReponseDTO> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        return employeeRepository.findAllByStatusTrue(pageRequest).toList()
                .stream()
                .map(EmployeeMapper::convertEmployeeToEmployeeResponseDTO)
                .collect(Collectors.toList());
    }

    public EmployeeReponseDTO insert(EmployeeRequestDTO employeeDTO) {
        Employee employee = EmployeeMapper.convertEmployeeRequestDTOToEmployee(employeeDTO);
        return EmployeeMapper.convertEmployeeToEmployeeResponseDTO(employeeRepository.save(employee));
    }

    public void delete(int id) {
        Employee employee = employeeRepository.findById(id).orElseThrow(EmployeeNotFoundException::new);
        employee.setStatus(false);
        employeeRepository.save(employee);
    }
}
