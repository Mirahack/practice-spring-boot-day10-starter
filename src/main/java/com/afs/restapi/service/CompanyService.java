package com.afs.restapi.service;

import com.afs.restapi.dto.CompanyRequestDTO;
import com.afs.restapi.dto.CompanyResponseDTO;
import com.afs.restapi.dto.EmployeeReponseDTO;
import com.afs.restapi.entity.Company;
import com.afs.restapi.exception.CompanyNotFoundException;
import com.afs.restapi.mapper.CompanyMapper;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.CompanyRepository;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CompanyService {
    private final CompanyRepository companyRepository;
    private final EmployeeRepository employeeRepository;

    public CompanyService(CompanyRepository companyRepository, EmployeeRepository employeeRepository) {
        this.companyRepository = companyRepository;
        this.employeeRepository = employeeRepository;
    }

    public List<CompanyResponseDTO> getAll() {
        List<Company> companyList = companyRepository.findAll();
        return companyList.stream().map(CompanyMapper::convertCompanyToCompanyResponseDTO).collect(Collectors.toList());
    }

    public List<CompanyResponseDTO> getAll(Integer page, Integer pageSize) {
        return companyRepository
                .findAll(PageRequest.of(page, pageSize))
                .map(CompanyMapper::convertCompanyToCompanyResponseDTO)
                .toList();
    }

    public CompanyResponseDTO findById(Integer companyId) {
        return companyRepository
                .findById(companyId)
                .map(CompanyMapper::convertCompanyToCompanyResponseDTO)
                .orElseThrow(CompanyNotFoundException::new);
    }

    public CompanyResponseDTO create(CompanyRequestDTO companyRequestDTO) {
        Company company = CompanyMapper.convertCompanyRequestDTOToCompany(companyRequestDTO);
        return CompanyMapper.convertCompanyToCompanyResponseDTO(companyRepository.save(company));
    }

    public void deleteCompany(Integer companyId) {
        Optional<Company> company = companyRepository.findById(companyId);
        company.ifPresent(companyRepository::delete);
    }

    public CompanyResponseDTO update(Integer companyId, CompanyRequestDTO updatingCompany) {
        Company company = companyRepository.findById(companyId)
                .orElseThrow(CompanyNotFoundException::new);
        if (updatingCompany.getName() != null) {
            company.setName(updatingCompany.getName());
        }
        return CompanyMapper.convertCompanyToCompanyResponseDTO(companyRepository.save(company));
    }

    public List<EmployeeReponseDTO> getEmployees(Integer companyId) {
        return companyRepository
                .findById(companyId)
                .orElseThrow(CompanyNotFoundException::new)
                .getEmployees()
                .stream()
                .map(EmployeeMapper::convertEmployeeToEmployeeResponseDTO)
                .collect(Collectors.toList());
    }
}
