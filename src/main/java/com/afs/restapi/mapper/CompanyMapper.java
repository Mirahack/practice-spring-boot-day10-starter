package com.afs.restapi.mapper;

import com.afs.restapi.dto.CompanyRequestDTO;
import com.afs.restapi.dto.CompanyResponseDTO;
import com.afs.restapi.entity.Company;
import com.afs.restapi.entity.Employee;
import org.springframework.beans.BeanUtils;

import java.util.List;
import java.util.stream.Collectors;

public class CompanyMapper {
    public static CompanyResponseDTO convertCompanyToCompanyResponseDTO(Company company) {
        CompanyResponseDTO companyResponseDTO = new CompanyResponseDTO();
        BeanUtils.copyProperties(company, companyResponseDTO);
        companyResponseDTO.setEmployeeNumber(company.getEmployees().size());
        return companyResponseDTO;
    }

    public static Company convertCompanyRequestDTOToCompany(CompanyRequestDTO companyRequestDTO) {
        Company company = new Company();
        BeanUtils.copyProperties(companyRequestDTO, company);
        List<Employee> employeeList = companyRequestDTO.getEmployees()
                .stream()
                .map(EmployeeMapper::convertEmployeeRequestDTOToEmployee)
                .collect(Collectors.toList());
        company.setEmployees(employeeList);
        return company;
    }
}
