package com.afs.restapi.mapper;

import com.afs.restapi.dto.EmployeeReponseDTO;
import com.afs.restapi.dto.EmployeeRequestDTO;
import com.afs.restapi.entity.Employee;
import org.springframework.beans.BeanUtils;

public class EmployeeMapper {
    public static Employee convertEmployeeRequestDTOToEmployee(EmployeeRequestDTO employeeDTO) {
        Employee employee = new Employee();
        BeanUtils.copyProperties(employeeDTO, employee);
        return employee;
    }

    public static EmployeeReponseDTO convertEmployeeToEmployeeResponseDTO(Employee employee) {
        EmployeeReponseDTO employeeReponseDTO = new EmployeeReponseDTO();
        BeanUtils.copyProperties(employee, employeeReponseDTO);
        return employeeReponseDTO;
    }
}
